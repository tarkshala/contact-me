package com.inbhiwadi.model;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Contact details of a potential opportunity.
 * <p>
 * Created by Kuldeep Yadav on 04/12/16.
 */
@Entity
@Table(name = "OPPORTUNITY")
public class Opportunity extends BaseEntity {

    /**
     * Name of the person
     */
    @Column(name = "name")
    private String name;

    /**
     * Phone number
     */
    @Column(name = "phone")
    private String phone;

    /**
     * Email id of person
     */
    @Column(name = "email")
    private String email;

    /**
     * Message by the person
     */
    @Column(name = "message")
    private String message;

    /**
     * Set of tags related
     */
    @ElementCollection
    @CollectionTable(name = "OPPORTUNITY_TAGS")
    private Set<String> tags;

    public Opportunity() {
        this.tags = new HashSet<String>();
    }

    /**
     * Get name of the person
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Set name of the person
     *
     * @param name name of the person
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get phone number of the person.
     *
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Set phone number of the person
     *
     * @param phone phone number
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Get email id of the person
     *
     * @return email id
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set email id of the person
     *
     * @param email email id
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get opportunity message
     *
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set opportunity message.
     *
     * @param message opportunity message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Get tags related to opportunity.
     *
     * @return Set of tags
     */
    public Set<String> getTags() {
        return tags;
    }

    /**
     * Set tags for opportunity.
     *
     * @param tags set
     */
    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    /**
     * Add a tag to opportunity.
     * <p>
     * Note: adding same tag multiple times will be counted only once.
     *
     * @param tag tag to be added
     */
    public void addTag(String tag) {
        this.tags.add(tag);
    }
}

package com.inbhiwadi.model;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Tag used to label entities.
 * <p>
 * Created by Kuldeep Yadav on 05/12/16.
 */
@Entity(name = "Tag")
public class Tag {

    /**
     * Tag name
     */
    @Column
    private String name;

    /**
     * Get tag name.
     *
     * @return name of tag
     */
    public String getName() {
        return name;
    }

    /**
     * Set name of tag.
     *
     * @param name name of tag
     */
    public void setName(String name) {
        this.name = name;
    }
}

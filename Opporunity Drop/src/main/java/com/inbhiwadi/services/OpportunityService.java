package com.inbhiwadi.services;

import com.inbhiwadi.dao.OpportunityDao;
import com.inbhiwadi.model.Opportunity;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Opportunity web services.
 * <p>
 * Created by Kuldeep Yadav on 06/12/16.
 */
@Path("/opportunity")
public class OpportunityService {

    @GET
    public String greet() {
        return "Welcome to InBhiwadi Opportunity services";
    }

    @POST
    @Path("/create")
    public Response create(Opportunity opportunity) {

        OpportunityDao dao = new OpportunityDao();
        dao.create(opportunity);
        return Response.accepted("Opportunity created").build();
    }
}

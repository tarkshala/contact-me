package com.inbhiwadi.dao;

import com.inbhiwadi.dao.hibernate.SessionFactoryProvider;
import org.hibernate.Session;

import java.io.Serializable;

/**
 * Basic CRUD operations supported by all entities which are
 * instance of {@code com.restroshop.dao.model.BaseEntity}.
 * <p/>
 * Created by Kuldeep Yadav on 4-Dec-16.
 */
public abstract class EntityDao<E> {

    /**
     * Create entity
     *
     * @return id of the entity saved in database
     */
    public Serializable create(E entity) {
        Session session = SessionFactoryProvider.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Serializable id = session.save(entity);
            session.getTransaction().commit();
            return id;
        } finally {
            session.close();
        }
    }

    /**
     * Read an entity.
     *
     * @param id id of the entity.
     * @return the entity
     */
    public abstract E read(Serializable id);

    /**
     * Update an existing entity.
     *
     * @param entity modified instance of entity.
     */
    public void update(E entity) {
        Session session = SessionFactoryProvider.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }

    /**
     * Delete an entity.
     *
     * @param id of the entity.
     */
    public abstract void delete(Serializable id);
}
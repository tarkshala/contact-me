package com.inbhiwadi.dao;

import com.inbhiwadi.dao.hibernate.SessionFactoryProvider;
import com.inbhiwadi.model.Opportunity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.io.Serializable;

/**
 * DAO for entity {@link com.inbhiwadi.model.Opportunity}.
 * <p>
 * Created by Kuldeep Yadav on 04/12/16.
 */
public class OpportunityDao extends EntityDao<Opportunity> {

    /**
     * Session factory provider.
     */
    private SessionFactory sessionFactory = SessionFactoryProvider.getSessionFactory();

    /**
     * Read opportunity.
     *
     * @param id id of the opportunity.
     * @return opportunity with given id
     */
    public Opportunity read(Serializable id) {

        Session session = sessionFactory.openSession();
        try {
            return session.get(Opportunity.class, id);
        } finally {
            session.close();
        }
    }

    /**
     * Delete entity with given id.
     *
     * @param id of the entity.
     */
    public void delete(Serializable id) {

        Session session = sessionFactory.openSession();

        try {
            Opportunity opportunity = read(id);
            session.beginTransaction();
            session.delete(opportunity);
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }
}

package com.inbhiwadi.dao.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 * Hibernate Session factory provider
 * <p>
 * Created by Kuldeep Yadav on 04/12/16.
 */
public class SessionFactoryProvider {

    /**
     * The session factory
     */
    private static final SessionFactory sessionFactory = buildSessionFactory();

    /**
     * Build session factory.
     *
     * @return session factory
     */
    private static SessionFactory buildSessionFactory() {
        try {
            final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                    .configure()
                    .build();

            return new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * Get session factory instance.
     *
     * @return session factory.
     */
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * Gracefully close all sessions
     */
    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }
}
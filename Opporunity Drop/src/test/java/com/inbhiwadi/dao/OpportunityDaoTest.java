package com.inbhiwadi.dao;

import com.inbhiwadi.model.Opportunity;
import org.junit.Test;

import java.io.Serializable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Junit test for {@link OpportunityDao}.
 * <p>
 * Created by Kuldeep Yadav on 04/12/16.
 */
public class OpportunityDaoTest {

    OpportunityDao dao = new OpportunityDao();

    @Test
    public void create() throws Exception {
        Opportunity opportunity = new Opportunity();
        opportunity.setName("Kuldeep");
        opportunity.setPhone("9416214587");
        opportunity.setEmail("kuldeep5534@gmail.com");
        opportunity.setMessage("Hey! Can you please mail me fee structure.");
        opportunity.addTag("Pathshala");
        opportunity.addTag("kalam");
        Serializable id = dao.create(opportunity);
        Opportunity opportunityInDB = dao.read(id);
        assertEquals(opportunity.getName(), opportunityInDB.getName());
        Thread.sleep(5000);
        opportunityInDB.setName("Kuldeep Yadav");
        dao.update(opportunityInDB);
    }

    @Test
    public void read() throws Exception {
        // read is covered in create()
        create();
    }

    @Test
    public void delete() throws Exception {
        Opportunity opportunity = new Opportunity();
        opportunity.setName("Kuldeep");
        opportunity.setPhone("9416214587");
        opportunity.setEmail("kuldeep5534@gmail.com");
        opportunity.setMessage("Hey! Can you please mail me fee structure.");
        Serializable id = dao.create(opportunity);
        dao.delete(id);
        Opportunity opportunityInDB = dao.read(id);
        assertNull(opportunityInDB);
    }

    @Test
    public void update() throws Exception {

    }
}